<?php
/**
 * Created by PhpStorm.
 * User: zhc
 * Date: 2019/10/28
 * Time: 17:20
 */

namespace Logic;


use think\Db;
use think\Exception;
use exception\ResponsableException;
use exception\LogicException;
class ChatLogLogic
{
  public static function addChatLog($data){
        return Db::name('chat_log')->insertGetId([
            'from_id' => $data['from_id'],
            'from_name' => $data['from_name'],
            'from_avatar' => $data['from_avatar'],
            'to_id' => $data['to_id'],
            'to_name' => $data['to_name'],
            'to_avatar' => $data['to_avatar'],
            'message' => htmlspecialchars($data['message']),
            'create_time' => date('Y-m-d H:i:s')
        ]);
  }
  public  static  function  updateSendStatus($logId,$status){
          return Db::name('chat_log')->where('log_id',$logId)->update([
              'send_status'=>$status,
              'create_time'=>date('Y-m-d H:i:s')
          ]);
  }

    /**
     * 分页获聊天记录
     * @param $param
     * @return boolean
     */
    public static function getChatLogList($vid='',$kefu_name='',$limit = 20)
    {
        try {
            if($vid and $kefu_name =='' ){
                $map1 = [
                    ['from_id', '=',$vid],
                ];
                $map2 = [
                    ['to_id', '=', $vid],
                ];
            }else if($vid and $kefu_name){
                $map1 = [
                    ['from_id', '=',$vid],
                    ['to_name', '=',$kefu_name],
                ];
                $map2 = [
                    ['from_name', '=', $kefu_name],
                    ['to_id', '=', $vid],
                ];
            }else if($vid =='' and $kefu_name){
                $map1 = [
                    ['from_name', '=',$kefu_name],
                ];
                $map2 = [
                    ['to_name', '=', $kefu_name],
                ];
            }else {
                $map1=['log_id','>',0];
                $map2=['log_id','>',0];
            }
            $list = Db::name('chat_log')->whereOr([$map1,$map2])->order('log_id desc')->paginate($limit);
            return $list;
        } catch (Exception $e) {
            throw new ResponsableException($e->getMessage(), 404);
        }

    }
}
